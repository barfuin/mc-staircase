/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase;

import java.util.ArrayList;
import java.util.List;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.WorldMock;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class BreakBlockFinderTest
{
    private static ServerMock server;

    private static StaircasePlugin plugin;



    @BeforeAll
    public static void setUp()
    {
        server = MockBukkit.mock();
        plugin = MockBukkit.load(StaircasePlugin.class);
    }



    @AfterAll
    public static void tearDown()
    {
        MockBukkit.unmock();
    }



    @Test
    public void testSingleBlock()
    {
        // Arrange -> prepare test
        World world = new WorldMock(Material.DIRT, 5);
        Block block = world.getBlockAt(1, 5, 1);
        block.setType(Material.NETHERRACK);

        // Act -> perform the test action
        BreakBlockFinder underTest = new BreakBlockFinder(block.getLocation());
        List<Location> actual = underTest.calculate();

        // Assert -> check results
        List<Location> expected = List.of(new Location(world, 1, 5, 1));
        Assertions.assertIterableEquals(expected, actual);
    }



    @Test
    public void testSingleStair()
    {

        //Arrange
        World world = new WorldMock(Material.DIRT, 5);
        List<Location> expected = buildStair(world);

        //Act
        BreakBlockFinder underTest = new BreakBlockFinder(expected.get(0));
        List<Location> actual = underTest.calculate();

        //Assert
        Assertions.assertIterableEquals(expected, actual);
    }



    private List<Location> buildStair(World pWorld)
    {
        List<Location> result = new ArrayList<>();
        int x = 0;
        final int z = 0;

        for (int y = 6; y < 16; y++) {
            Block block = pWorld.getBlockAt(x, y, z);
            block.setType(Material.NETHERRACK);
            result.add(block.getLocation());

            x++;

            block = pWorld.getBlockAt(x, y, z);
            block.setType(Material.NETHERRACK);
            result.add(block.getLocation());
        }

        return result;
    }
}
