/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase.listener;

import org.barfuin.minecraft.staircase.BreakBlockFinder;
import org.barfuin.minecraft.staircase.ItemWandOfDeconstruction;
import org.barfuin.minecraft.staircase.StaircasePlugin;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.WorldMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import org.barfuin.minecraft.staircase.UndoData;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class InteractListenerTest
{
    private static StaircasePlugin plugin;

    private static ServerMock server;



    @BeforeAll
    public static void setUp()
    {
        server = MockBukkit.mock();
        plugin = MockBukkit.load(StaircasePlugin.class);
    }



    @AfterAll
    public static void tearDown()
    {
        MockBukkit.unmock();
    }



    @Test
    public void testLeftClickBlock()
    {

        //Arrange
        World world = new WorldMock(Material.DIRT, 5);
        Block block = world.getBlockAt(1, 6, 1);
        block.setType(Material.NETHERRACK);

        PlayerMock player = server.addPlayer();

        PlayerInteractEvent event =
            new PlayerInteractEvent(player, Action.LEFT_CLICK_BLOCK, new ItemStack(Material.NETHERRACK), block,
                BlockFace.NORTH);

        //Act
        server.getPluginManager().callEvent(event);

        //Assert
        //Nothing should happen
    }



    @Test
    public void testRightClickBlock()
    {

        //Arrange
        World world = new WorldMock(Material.DIRT, 5);
        Block block = world.getBlockAt(1, 6, 1);
        block.setType(Material.NETHERRACK);

        PlayerMock player = server.addPlayer();

        PlayerInteractEvent event =
            new PlayerInteractEvent(player, Action.RIGHT_CLICK_BLOCK, new ItemStack(Material.NETHERRACK),
                block,
                BlockFace.NORTH);

        //Act
        server.getPluginManager().callEvent(event);

        //Assert
        //Nothing should happen

    }



    @Test
    public void testLeftClickBlockWithItem()
    {

        //Arrange
        World world = new WorldMock(Material.DIRT, 5);
        Block block = world.getBlockAt(1, 6, 1);
        block.setType(Material.NETHERRACK);

        PlayerMock player = server.addPlayer();

        PlayerInteractEvent event =
            new PlayerInteractEvent(player, Action.LEFT_CLICK_BLOCK, new ItemWandOfDeconstruction().createItem(),
                block,
                BlockFace.NORTH);

        //Act
        server.getPluginManager().callEvent(event);

        //Assert
        //Nothing should happen

    }



    @SuppressWarnings("deprecation")
    @Test
    public void testRightClickBlockWithItem()
    {

        //Arrange
        World world = new WorldMock(Material.DIRT, 5);
        Block block = world.getBlockAt(1, 6, 1);
        block.setType(Material.NETHERRACK);

        PlayerMock player = server.addPlayer();

        PlayerInteractEvent event =
            new PlayerInteractEvent(player, Action.RIGHT_CLICK_BLOCK, new ItemWandOfDeconstruction().createItem(),
                block,
                BlockFace.NORTH);

        //Act
        server.getPluginManager().callEvent(event);

        //Assert
        String actualTitle = player.nextTitle();
        String expectedTitle = ChatColor.DARK_PURPLE + "About to break " + ChatColor.GOLD + 1 +
            ChatColor.DARK_PURPLE + " blocks";
        Assertions.assertEquals(expectedTitle, actualTitle);
    }



    @SuppressWarnings("deprecation")
    @Test
    public void testTooManyNeighbours()
    {

        //Arrange
        World world = new WorldMock(Material.DIRT, 5);
        Block block = world.getBlockAt(1, 5, 1);
        PlayerMock player = server.addPlayer();

        PlayerInteractEvent event =
            new PlayerInteractEvent(player, Action.RIGHT_CLICK_BLOCK, new ItemWandOfDeconstruction().createItem(),
                block,
                BlockFace.NORTH);

        //Act
        server.getPluginManager().callEvent(event);

        //Assert
        String expectedMessage =
            ChatColor.GRAY + "[" + ChatColor.AQUA + ItemWandOfDeconstruction.WOD_NAME + ChatColor.GRAY
                + "] It does not seem to work here. Try somewhere else.";

        String actualMessage = player.nextMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }



    @Test
    public void testdoubleRightClickWithItem()
    {

        //Arrange
        World world = new WorldMock(Material.DIRT, 5);
        List<Location> expected = buildStair(world);
        PlayerMock player = server.addPlayer();
        UndoData undoData;

        PlayerInteractEvent event =
            new PlayerInteractEvent(player, Action.RIGHT_CLICK_BLOCK, new ItemWandOfDeconstruction().createItem(),
                expected.get(0).getBlock(),
                BlockFace.NORTH);

        //Act
        server.getPluginManager().callEvent(event);
        server.getPluginManager().callEvent(event);

        //Assert
        if (plugin.getUndoData(player.getUniqueId()) == null) {
            return;
        }
        undoData = plugin.getUndoData(player.getUniqueId());
        List<Location> actual = Objects.requireNonNull(undoData).getLocations();

        Assertions.assertIterableEquals(expected, actual);
    }



    private List<Location> buildStair(World pWorld)
    {
        List<Location> result = new ArrayList<>();
        int x = 0;
        final int z = 0;

        for (int y = 6; y < 16; y++) {
            Block block = pWorld.getBlockAt(x, y, z);
            block.setType(Material.NETHERRACK);
            result.add(block.getLocation());

            x++;

            block = pWorld.getBlockAt(x, y, z);
            block.setType(Material.NETHERRACK);
            result.add(block.getLocation());
        }

        return result;
    }


    @Test
    public void testUndo()
    {

        //Arrange
        World world = new WorldMock(Material.DIRT, 5);
        List<Location> expected = buildStair2(world);
        PlayerMock player = server.addPlayer();
        UndoData undoData;

        PlayerInteractEvent event =
            new PlayerInteractEvent(player, Action.RIGHT_CLICK_BLOCK, new ItemWandOfDeconstruction().createItem(),
                expected.get(0).getBlock(),
                BlockFace.NORTH);

        //Act
        server.getPluginManager().callEvent(event);
        server.getPluginManager().callEvent(event);

        if (plugin.getUndoData(player.getUniqueId()) == null) {
            return;
        }
        undoData = plugin.getUndoData(player.getUniqueId());
        List<Location> actual = Objects.requireNonNull(undoData).getLocations();

        player.performCommand("/wundo");

        //Assert

        for (Location loc : actual) {
            if (!loc.getBlock().getType().equals(Material.NETHERRACK)){
                actual.remove(loc);
            }
        }

        Assertions.assertIterableEquals(expected, actual);
    }



    private List<Location> buildStair2(World pWorld)
    {
        List<Location> result = new ArrayList<>();
        int x = 0;
        final int z = 0;

        for (int y = 6; y < 16; y++) {
            Block block = pWorld.getBlockAt(x, y, z);
            block.setType(Material.NETHERRACK);
            result.add(block.getLocation());

            x++;

            block = pWorld.getBlockAt(x, y, z);
            block.setType(Material.NETHERRACK);
            result.add(block.getLocation());
        }

        return result;
    }
}
