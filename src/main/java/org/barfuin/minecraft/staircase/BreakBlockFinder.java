/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import javax.annotation.Nonnull;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.util.Vector;


public class BreakBlockFinder
{
    /** Algorithm stops when a block has more than this number of potential neighbors */
    private static final int MAX_NEIGHBORS = 4;

    private static final List<Vector> NEIGHBOR_VEC = List.of(
        new Vector(1, 0, 0),
        new Vector(-1, 0, 0),
        new Vector(0, 0, 1),
        new Vector(0, 0, -1),
        new Vector(1, 0, 1),
        new Vector(1, 0, -1),
        new Vector(-1, 0, 1),
        new Vector(-1, 0, -1),
        new Vector(1, 1, 0),
        new Vector(-1, 1, 0),
        new Vector(0, 1, 1),
        new Vector(0, 1, -1),
        new Vector(1, 1, 1),
        new Vector(1, 1, -1),
        new Vector(-1, 1, 1),
        new Vector(-1, 1, -1),
        new Vector(0, 1, 0),
        new Vector(1, -1, 0),
        new Vector(-1, -1, 0),
        new Vector(0, -1, 1),
        new Vector(0, -1, -1),
        new Vector(1, -1, 1),
        new Vector(1, -1, -1),
        new Vector(-1, -1, 1),
        new Vector(-1, -1, -1),
        new Vector(0, -1, 0)
    );

    private final Location start;

    private final Set<Location> foundBlocks = new TreeSet<>(new RoughLocationComparator());



    public BreakBlockFinder(@Nonnull final Location pStart)
    {
        start = Objects.requireNonNull(pStart);
    }



    private boolean isBlockAlreadyFound(@Nonnull final Location pLoc)
    {
        return foundBlocks.contains(pLoc);
    }



    @Nonnull
    private List<Location> getNeighbors(@Nonnull final Location pLoc)
    {
        final Material orgMaterial = pLoc.getBlock().getType();
        List<Location> result = new ArrayList<>();

        for (Vector v : NEIGHBOR_VEC) {
            Location neighbor = pLoc.clone().add(v);
            Material material = neighbor.getBlock().getType();
            if (material == orgMaterial && !isBlockAlreadyFound(neighbor)) {
                result.add(neighbor);
            }
        }
        return result;
    }



    @Nonnull
    public List<Location> calculate()
    {
        calculate(start);
        List<Location> result = new ArrayList<>(foundBlocks);
        sortByDistanceFromStart(result);
        return result;
    }



    private void calculate(@Nonnull final Location pLoc)
    {
        if (!isBlockAlreadyFound(pLoc)) {
            List<Location> neighbors = getNeighbors(pLoc);
            if (neighbors.size() <= MAX_NEIGHBORS) {
                foundBlocks.add(pLoc);
                neighbors.forEach(this::calculate);
            }
        }
    }



    private void sortByDistanceFromStart(@Nonnull final List<Location> pList)
    {
        pList.sort(new DistanceComparator(start));
    }
}
