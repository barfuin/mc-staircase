/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase;

import java.util.Comparator;

import javax.annotation.Nullable;

import org.bukkit.Location;


public class RoughLocationComparator
    implements Comparator<Location>
{
    @Override
    public int compare(@Nullable final Location pLoc1, @Nullable final Location pLoc2)
    {
        int result = 0;
        if (pLoc1 == null) {
            if (pLoc2 != null) {
                result = 1;
            }
        }
        else {
            if (pLoc2 == null) {
                result = -1;
            }
            else if (!Util.isSameBlock(pLoc1, pLoc2)) {
                result = pLoc1.hashCode() < pLoc2.hashCode() ? -1 : 1;
            }
        }
        return result;
    }
}
