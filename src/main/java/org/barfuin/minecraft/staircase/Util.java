/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase;

import javax.annotation.Nullable;

import org.bukkit.Location;


public final class Util
{
    private Util()
    {
        // utility class
    }



    public static boolean isSameBlock(@Nullable final Location loc1, @Nullable final Location loc2)
    {
        return (loc1 == null && loc2 == null)
            || (loc1 != null && loc2 != null
            && loc1.getBlockX() == loc2.getBlockX()
            && loc1.getBlockY() == loc2.getBlockY()
            && loc1.getBlockZ() == loc2.getBlockZ());
    }
}
