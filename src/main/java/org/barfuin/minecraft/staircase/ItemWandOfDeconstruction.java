/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class ItemWandOfDeconstruction
{
    /** Name of the Wand of Deconstruction */
    public static final String WOD_NAME = "Wand of Deconstruction";
    public static final String WOD_KEY = "wand_of_deconstruction";


    public ItemStack createItem()
    {
        ItemStack item = new ItemStack(Material.STICK);
        ItemMeta meta = item.getItemMeta();
        if (meta != null) {
            meta.addEnchant(Enchantment.LOYALTY, 5, true);
            meta.setUnbreakable(true);
            meta.setDisplayName(WOD_NAME);

            List<String> lore = new ArrayList<>();
            lore.add(ChatColor.DARK_PURPLE + "The " + ChatColor.GOLD + WOD_NAME
                + ChatColor.DARK_PURPLE + ", a conduit of the void's");
            lore.add("unfathomable abyss. Forged where light dare not tread,");
            lore.add("under the gaze of a waning moon, it conveys the power to");
            lore.add("envelop the tangible in the cloak of oblivion.");
            meta.setLore(lore);
            item.setItemMeta(meta);
        }
        return item;
    }
}
