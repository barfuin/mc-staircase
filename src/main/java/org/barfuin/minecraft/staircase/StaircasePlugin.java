/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.barfuin.minecraft.staircase.command.GetWodCommand;
import org.barfuin.minecraft.staircase.command.WodUndoCommand;
import org.barfuin.minecraft.staircase.listener.InteractListener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.PluginCommand;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


public class StaircasePlugin
    extends JavaPlugin
{
    private final Map<UUID, UndoData> undoData = new HashMap<>();

    private final Map<UUID, Boolean> wodBreakingInProgress = new HashMap<>();
    private BossBar progressBar;

    //TODO Config for max block breaknumber
    //TODO Config for breakspeed
    //TODO cancel Command
    //TODO Make Break algorithm configurable

    @Override
    public void onEnable()
    {
        PluginCommand cmd = getCommand("WandOfDeconstruction");
        if (cmd != null) {
            cmd.setExecutor(new GetWodCommand(this));
        }

        PluginCommand cmd2 = getCommand("WodUndo");
        if (cmd2 != null) {
            cmd2.setExecutor(new WodUndoCommand(this));
        }

        addWodRecipe();
        setUpProgressBar();

        getServer().getPluginManager().registerEvents(new InteractListener(this), this);
        getServer().getConsoleSender().sendMessage("[" + getName() + "] Plugin " + ChatColor.GREEN + "enabled");
    }

    private void setUpProgressBar() {
        progressBar = Bukkit.createBossBar(ChatColor.WHITE + "Breaking-Progress", BarColor.GREEN, BarStyle.SOLID);
    }


    @SuppressWarnings("deprecation")
    private void addWodRecipe()
    {
        ShapedRecipe wodRecipe = new ShapedRecipe(new NamespacedKey(this, ItemWandOfDeconstruction.WOD_KEY),
            new ItemWandOfDeconstruction().createItem());
        wodRecipe.shape(" N ", "WSK", " T ");
        wodRecipe.setIngredient('N', Material.NETHERITE_PICKAXE);
        wodRecipe.setIngredient('W', new MaterialData(Material.POTION));
        wodRecipe.setIngredient('S', Material.STICK);
        wodRecipe.setIngredient('K', Material.CAKE);
        wodRecipe.setIngredient('T', Material.TOTEM_OF_UNDYING);
        getServer().addRecipe(wodRecipe);
    }



    @Override
    public void onDisable()
    {
        getServer().getConsoleSender().sendMessage("[" + getName() + "] Plugin " + ChatColor.RED + "disabled");
    }



    @CheckForNull
    public UndoData getUndoData(@Nonnull final UUID pPlayerUuid) {
        return undoData.get(pPlayerUuid);
    }



    public void setUndoData(@Nonnull final UUID pPlayerUuid ,@Nullable final UndoData pUndoData)
    {
        undoData.put(pPlayerUuid, pUndoData);
    }

    @Nonnull
    public BossBar getProgressBar() {
        return progressBar;
    }



    public boolean isWodBreakingInProgress(@Nonnull final UUID pPLayerUuid) {
        return wodBreakingInProgress.getOrDefault(pPLayerUuid, false);
    }

    public void setWodBreakingInProgress(@Nonnull final UUID pPlayerUuid, final boolean pValue){
        wodBreakingInProgress.put(pPlayerUuid, pValue);
    }
}
