/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;

import org.bukkit.Location;
import org.bukkit.Material;

public class UndoData
{
    private final List<Location> locations = new ArrayList<>();

    private Material material;



    public UndoData()
    {
        setMaterial(Material.AIR);
    }



    public UndoData(@Nonnull final List<Location> pLocations, @Nonnull final Material pMaterial)
    {
        this();
        setLocations(pLocations);
        setMaterial(pMaterial);
    }



    @Nonnull
    public List<Location> getLocations()
    {
        return locations;
    }



    public void setLocations(@Nonnull final List<Location> pLocations)
    {
        locations.clear();
        locations.addAll(Objects.requireNonNull(pLocations));
    }



    @Nonnull
    public Material getMaterial()
    {
        return material;
    }



    public void setMaterial(@Nonnull final Material pMaterial)
    {
        material = Objects.requireNonNull(pMaterial);
    }
}
