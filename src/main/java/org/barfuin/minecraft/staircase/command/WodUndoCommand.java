/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase.command;

import javax.annotation.Nonnull;

import org.barfuin.minecraft.staircase.ItemWandOfDeconstruction;
import org.barfuin.minecraft.staircase.StaircasePlugin;
import org.barfuin.minecraft.staircase.UndoData;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WodUndoCommand
        implements CommandExecutor
{
    private final StaircasePlugin plugin;

    //TODO cancel breaking before undo


    public WodUndoCommand(StaircasePlugin staircasePlugin)
    {
        plugin = staircasePlugin;
    }



    @Override
    public boolean onCommand(@Nonnull final CommandSender pSender, @Nonnull final Command pCommand,
            @Nonnull final String pLabel, @Nonnull final String[] pArgs)
    {
        if (pSender instanceof Player player) {

            UndoData undoData = plugin.getUndoData(player.getUniqueId());

            if (undoData == null) {
                player.sendTitle("Nothing to undo", null, 2, 20, 2);
                player.playSound(player, Sound.ENTITY_ENDERMAN_TELEPORT, 5, 5);
                return true;
            }
            World world = player.getWorld();
            player.playSound(player, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 5, 5);

            for (Location location : undoData.getLocations()) {
                if (location.getBlock().getType().equals(Material.AIR)){
                    world.getBlockAt(location).setType(undoData.getMaterial());
                }
                //TODO remove from drops?
            }
            player.sendMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + ItemWandOfDeconstruction.WOD_NAME + ChatColor.GRAY
                    + "] " + ChatColor.GRAY + "Successfully undid the action");
        }
        return true;
    }
}
