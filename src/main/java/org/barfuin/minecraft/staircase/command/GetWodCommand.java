/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase.command;

import java.util.Objects;
import javax.annotation.Nonnull;

import org.barfuin.minecraft.staircase.ItemWandOfDeconstruction;
import org.barfuin.minecraft.staircase.StaircasePlugin;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GetWodCommand
        implements CommandExecutor
{
    private final StaircasePlugin plugin;



    public GetWodCommand(@Nonnull final StaircasePlugin pPlugin)
    {
        plugin = Objects.requireNonNull(pPlugin);
    }



    @Override
    public boolean onCommand(@Nonnull final CommandSender sender, @Nonnull final Command command,
            @Nonnull final String label, @Nonnull final String[] args)
    {
        if (sender instanceof Player player) {
            ItemWandOfDeconstruction item = new ItemWandOfDeconstruction();
            ItemStack itemToGet = item.createItem();

            if (!player.getInventory().contains(itemToGet)) {
                player.getInventory().addItem(itemToGet);
            }
        }
        else {
            plugin.getServer().getConsoleSender().sendMessage(
                    ChatColor.WHITE + "Get " + ChatColor.GOLD + "your" + ChatColor.WHITE + " ass on the server, " +
                            "man!!!");
        }
        return true;
    }
}
