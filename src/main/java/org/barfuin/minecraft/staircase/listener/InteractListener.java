/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase.listener;

import java.util.*;

import org.barfuin.minecraft.staircase.*;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;


public class InteractListener
        implements Listener {
    private final Map<UUID, Location> toggleList = new HashMap<>();

    private static final List<Material> ILLEGAL_BLOCKS = List.of(Material.BEDROCK, Material.WATER, Material.LAVA,
            Material.AIR, Material.REINFORCED_DEEPSLATE);

    private final StaircasePlugin plugin;


    public InteractListener(final StaircasePlugin pPlugin) {
        plugin = pPlugin;
    }


    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        // TODO extract methods
        ItemStack item = e.getItem();
        ItemMeta meta = item != null ? item.getItemMeta() : null;
        String displayName = meta != null ? meta.getDisplayName() : null;
        Block clickedBlock = e.getClickedBlock();
        final Material blockType = clickedBlock != null ? clickedBlock.getType() : null;

        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && ItemWandOfDeconstruction.WOD_NAME.equalsIgnoreCase(displayName)) {

            if (!ILLEGAL_BLOCKS.contains(blockType)) {

                Player player = e.getPlayer();

                if (toggleList.containsKey(player.getUniqueId())
                        && Util.isSameBlock(toggleList.get(player.getUniqueId()),
                        clickedBlock != null ? clickedBlock.getLocation() : null)) {

                    if (plugin.isWodBreakingInProgress(player.getUniqueId())){
                        player.sendMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + ItemWandOfDeconstruction.WOD_NAME + ChatColor.GRAY
                            + "] The wand is busy. Wait a minute.");
                        return;
                    }

                    plugin.setWodBreakingInProgress(player.getUniqueId(), true);

                    toggleList.remove(player.getUniqueId());
                    final List<Location> breakingBlocks = new BreakBlockFinder(clickedBlock.getLocation()).calculate();
                    final Iterator<Location> bbIter = breakingBlocks.iterator();
                    player.playSound(player, Sound.ENTITY_PHANTOM_SWOOP, 4, 4);

                    BukkitRunnable runnable = new BukkitRunnable() {
                        int counter = breakingBlocks.size();
                        @Override
                        public void run() {
                            if (bbIter.hasNext()) {
                                Location loc = bbIter.next();
                                if (Objects.requireNonNull(loc.getWorld()).getBlockAt(loc).getType().equals(blockType)) {
                                    loc.getBlock().breakNaturally();
                                }
                            } else {
                                plugin.setUndoData(player.getUniqueId() ,new UndoData(breakingBlocks, blockType));
                                plugin.getProgressBar().removePlayer(player);
                                plugin.getProgressBar().setVisible(false);
                                plugin.setWodBreakingInProgress(player.getUniqueId(), false);
                                cancel();
                            }

                            updateProgress(player, counter, breakingBlocks.size());
                            counter--;

                        }
                    };
                    plugin.getProgressBar().addPlayer(player);
                    plugin.getProgressBar().setProgress(1.0f);
                    plugin.getProgressBar().setVisible(true);


                    runnable.runTaskTimer(plugin, 0, 2);

                } else if (clickedBlock != null) {
                    List<Location> breakingBlocks = new BreakBlockFinder(clickedBlock.getLocation()).calculate();

                    if (breakingBlocks.isEmpty()) {
                        player.playSound(player, Sound.ENTITY_ENDERMAN_TELEPORT, 5, 5);
                        player.sendMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + ItemWandOfDeconstruction.WOD_NAME + ChatColor.GRAY
                                + "] It does not seem to work here. Try somewhere else.");
                    } else {
                        toggleList.put(player.getUniqueId(), clickedBlock.getLocation());
                        player.playSound(player, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 5, 5);

                        player.sendTitle(ChatColor.DARK_PURPLE + "About to break " + ChatColor.GOLD + breakingBlocks.size() + ChatColor.DARK_PURPLE + " blocks",
                                ChatColor.WHITE + "Do you really mean it?", 10, 60, 20);
                    }
                }
            } else {
                Player player = e.getPlayer();
                player.playSound(player, Sound.ENTITY_ENDERMAN_TELEPORT, 5, 5);
                player.sendMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + ItemWandOfDeconstruction.WOD_NAME + ChatColor.GRAY
                        + "] You can´t break this type of block. Try somewhere else.");
            }
        }
    }

    private void updateProgress(Player player, int progress, int max){
        BossBar progressBar = plugin.getProgressBar();
        double percent = (double) progress / (double) max;

            progressBar.setProgress(percent);
    }
}
