/*
 * mc-staircase - A Minecraft plugin for staircase management
 * Copyright (c) the mc-staircase contributors
 *
 * This program is free software: You can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package org.barfuin.minecraft.staircase;

import java.util.Comparator;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.bukkit.Location;


/**
 * Order locations by their distance from a block zero.
 */
public class DistanceComparator
    implements Comparator<Location>
{
    private final Location zero;



    public DistanceComparator(@Nonnull final Location pZero)
    {
        zero = Objects.requireNonNull(pZero);
    }



    @Override
    public int compare(@Nullable final Location loc1, @Nullable final Location loc2)
    {
        int result = 0;
        if (loc1 == null) {
            if (loc2 != null) {
                result = 1;
            }
        }
        else {
            if (loc2 == null) {
                result = -1;
            }
            else {
                double dist1 = Math.abs(zero.distanceSquared(loc1));
                double dist2 = Math.abs(zero.distanceSquared(loc2));
                result = Double.compare(dist1, dist2);
            }
        }
        return result;
    }
}
